const Discord = require('discord.js');
const client = new Discord.Client();

const mysql = require('mysql');
const sql = mysql.createConnection({
  host     : 'localhost',
  user     : '',
  password : '',
  database : ''
});
 
sql.connect(function(err) {
  if (err) {
    console.error('error connecting: ' + err.stack);
    return;
  }
 
  console.log('connected as id ' + sql.threadId);
});

const apiToken = '';

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', msg => {
  if(msg.author == client.user) { return }
  
  // DM Commands
  if(msg.author.dmChannel == msg.channel && msg.channel.type == 'dm') {
console.log(msg);
    var q    = 'SELECT creator,step FROM guilds WHERE creator = ' + sql.escape(msg.author.id);
    sql.query(q, function (error, results, fields) {
      if (error) throw error;
        if(results != '') {
          console.log(results);
          console.log(results[0]['step']);
          switch(results[0]['step']) {
            case 0:
              sql.query("UPDATE `guilds` SET `step` = 1, `name` = ? WHERE `creator` = ? LIMIT 1", [ sql.escape(msg.content), msg.author.id])
              msg.author.send("That's a great name! If it's too difficult to type on standard keyboards; please ensure your contact information is accurate!\n\n Now, what type of Guild is it? Is it Competitive, Casual, Welfare, Supporting, or other?\n_You can enter whatever you'd like in reply_")
            break;
            case 1:
              // Guild Type
              sql.query("UPDATE `guilds` SET `step` = 2, `type` = ? WHERE `creator` = ? LIMIT 1", [ sql.escape(msg.content), msg.author.id])
              msg.author.send("Sounds good! It's always great that we have guilds anyone can find a home within.\nNow, please give us a description of your Guild. This is where you'll want to tell us about your Gulid, any events you run, and your typical players. We'll ask about your Minimum, Languages, and Chats after this.")
            break;
            case 2:
              //Guild Description
              sql.query("UPDATE `guilds` SET `step` = 3, `descript` = ? WHERE `creator` = ? LIMIT 1", [ sql.escape(msg.content), msg.author.id])
              msg.author.send("We've saved your description! So, when it comes to guild match weekends; whats your minimum requirement for guild members?")
            break;
            case 3:
              // Guild Minimum
              sql.query("UPDATE `guilds` SET `step` = 4, `min` = ? WHERE `creator` = ? LIMIT 1", [ sql.escape(msg.content), msg.author.id])
              msg.author.send("I like that minimum; hope we see you in the top 100 guilds! Always aim for the top 10 though :clap:\n\nNow onto details about your guild members. What languages does your guild speak primarily?")
            break;
            case 4:
              // Languages
              sql.query("UPDATE `guilds` SET `step` = 5, `langs` = ? WHERE `creator` = ? LIMIT 1", [ sql.escape(msg.content), msg.author.id])
              msg.author.send("There's always something for everyone! Now... if someone joins your guild. How will they communicate with the other members? You're welcome to enter your Whatsapp / LINE numbers and whatever necessary.\n**Absolutely no private information though; this will be public**")
            break;
            case 5:
              // Chat Methods
              sql.query("UPDATE `guilds` SET `step` = 6, `chatmthds` = ? WHERE `creator` = ? LIMIT 1", [ sql.escape(msg.content), msg.author.id])
              msg.author.send("Awesome! Lastly, who should a potential member contact to get an invite or check their eligibility? You can put Discord usernames and the like here too; no private contact information.")
            break;
            case 6:
              // Contact / Preview
              sql.query("UPDATE `guilds` SET `step` = 7, `contact` = ? WHERE `creator` = ? LIMIT 1", [ sql.escape(msg.content), msg.author.id])
              msg.author.send("That's everything! We're done! Before we go posting this though; please take a peek at what it'll look like when its posted.\n\nIf it's okay, please reply 'okay'\n If it is not okay, please reply 'start over'")
                sql.query('SELECT * FROM `guilds` WHERE creator = ?', [msg.author.id], function (error, results, fields) {
                  if (error) throw error;
                    chatMsg = "```md\r\n                                    " + results[0]['name'].replace(/'/g, '').replace(/\\/g, '') + " \r\n\# Guild Type: " + results[0]['type'].replace(/'/g, '').replace(/\\/g, '') + "\r\n\# Description: " + results[0]['descript'].replace(/'/g, '').replace(/\\/g, '') + "\r\n\# Min. Contribution: " + results[0]['min'].replace(/'/g, '').replace(/\\/g, '') + "\r\n\# Languages: " + results[0]['langs'].replace(/'/g, '').replace(/\\/g, '') + "\r\n\# Communication Platform: " + results[0]['chatmthds'].replace(/'/g, '').replace(/\\/g, '') + " \r\n\# Who to Contact: " + results[0]['contact'].replace(/'/g, '').replace(/\\/g, '') + "\r\n\# Last Updated: " + results[0]['lastupdate'] + " by @" + msg.author.username + "#" + msg.author.discriminator + "```";
                    sql.query("UPDATE `guilds` SET `discordmessage` = ? WHERE `creator` = ? LIMIT 1", [ chatMsg, msg.author.id]);
                  msg.author.send(chatMsg)
                  });
              break;
            case 7:
              // Confirm 
              if(msg.content == 'okay') {
                sql.query("UPDATE `guilds` SET `step` = 8, `confirmed` = 1 WHERE `creator` = ? LIMIT 1", [ msg.author.id])
                sql.query('SELECT `discordmessage` FROM `guilds` WHERE creator = ?', [msg.author.id], function (error, results, fields) {
                  if (error) throw error;
                  const chan = client.channels.cache.get('255320504322228224')
                    chan.send(results[0]['discordmessage']);
                });
                msg.author.send("Alright! We're all done here. Your Guild has been posted in the #guilds channel, you're welcome to contact us for a channel+role too.\n\n If you need to edit/delete your guild post, please contact a moderator as we continue developing this bot.")
              } else if (msg.content == 'start over') {
                sql.query("UPDATE `guilds` set `step` = 0 WHERE `creator` = ? LIMIT 1", [ msg.author.id ]);
                msg.author.send("Your guild data has been reset to start the process over again. With that said; I forgot... What's your guild's name again?");
              } else {
                msg.author.send("Please only reply either `okay` or `start over` to confirm the posting of your guild.");
              }
            break;
            case 8:
              msg.author.send("Alright! We're all done here. Your Guild has been posted in the #guilds channel, you're welcome to contact us for a channel+role too.\n\n If you need to edit/delete your guild post, please contact a moderator as we continue developing this bot.");
            break;
          }
        } else {
          msg.author.send("Welcome to the Game of Dice Guild Manager bot. Let's get your guild added to the Discord!\n\nLet's start with your guild name; what is it?")
          sql.query('INSERT INTO `guilds` (`creator`) VALUES( ? )', [ msg.author.id ])
        }
    });

    return;
  }

  function createEmbed(title, desc, image, color, fields, url='') {
    const embed = new Discord.MessageEmbed()
    .setColor(color)
    .setTitle(title)
    .setAuthor('Game of Dice :: Helper Bot', 
'https://cdn.discordapp.com/avatars/367495122277105674/70404af5519355850d569d36791fa798.png?size=128')
    .setDescription(desc)
    .setThumbnail(image)
    //.addFields(fields)
       /* { name: 'Time for Revenge!', value: ':star::star::star::star::star:' },
        { name: '\u200B', value: '\u200B' },
        { name: 'Description', value: 'A very old and useless card in 2021', inline: true 
},
        { name: 'Best used with', value: 'your mom', inline: true },
    )
    .addField('Should I buy?', 'Fuck no.', true) //single field*/
    //.setImage(image)
    .setTimestamp()
    .setFooter('Made by us; written by MyNode', 
'https://cdn.discordapp.com/emojis/816928970809409548.png?v=1');
  if(url != '') {
    embed.setURL(url);
  }
  if(fields.length != 0) {
  for (const [k, v] of Object.entries(fields)) {
      embed.addField({k: v})
    };
  }
    return embed
  }

  // Channel Commands
  if(msg.content.startsWith('!')) {
    switch(msg.content.replace('!', '').split(' ')[0]) {
      case 'newguild':
        msg.reply("We have sent you a new DM!")
        msg.author.send("Hey there! Reply to get started!")        
      break;
      case 'updateguild':
      break;
      case 'deleteguild':
      break;
      case 'guildlist':
      break;
      case 'lookup':

      /* 
      !lookup 6* day of odds
      !lookup collector day of odds
      !lookup 5D rica

      */
     
      if(msg.member.roles.cache.find(r => r.name === "Discord Admin")) {
        sql.query("UPDATE `users` set `rank` = 420 WHERE `id` = " + msg.author.id)
      } else if(msg.member.roles.cache.find(r => r.name === "Wiki Editor")){
        sql.query("UPDATE `users` set `rank` = 10 WHERE `id` = " + msg.author.id)
      } else {
        sql.query("UPDATE `users` set `rank` = 1 WHERE `id` = " + msg.author.id)
      }
      command = msg.content.replace('!lookup', '').split(' ') // We want just the string
      // Ghetto switch time
      allowedTypes = [
        '3*',
        '4*',
        '5*',
        '5G',
        '5P',
        '5D',
        '6*',
        '6G',
        '6G+',
        '6P',
        '6P+',
        'radiant',
        'black',
        'inf', 'infinity',
        'seasonal', // latest seasonal version
        'collector', 'col' // alias col
      ]
      console.log(command)
      //msg.channel.send(JSON.stringify(command))
      allowedTypes.forEach(v => {
        if(command.includes(v)) {
          type = v;
          endString = command.join(' ').replace(v, '').trim()
          var q = `SELECT * FROM items WHERE (name = ${sql.escape(endString)} or name LIKE "%${sql.escape(endString)}%") AND type = ${sql.escape(type)}`
          sql.query(q, function (error, results, fields) {
            if (error) throw error;
              if(results != '') {
                console.log(results[0]);
                //console.log(JSON.stringify(results));
                msg.channel.send(createEmbed(results[0]['type']+' '+results[0]['name'], results[0]['description'], results[0]['image'].replace(' ', '%20'), '#ff0000', {}, 'https://gameofdice.wiki/'+results[0]['class']+'/'+results[0]['type']+'-'+results[0]['name'].replace(' ', '-')+'/'));
                //msg.channel.send(createEmbed(results[0]['name'], results[0]['description'], results[0]['image'], '#ff0000', {name: "Some card", value: 'Taco'}, 'https://gameofdice.wiki/'+results[0]['class']+'/'+results[0]['type']+results[0]['name'].replace(' ', '-')+'/'));
             }
            });
        }
      })
      break;
      case 'hello':
        msg.author.send('Hi');
      break;
    }
    return;
  }

  if (msg.content === 'ping') {
    msg.reply('Pong!');
  }
});

client.login(apiToken);
